diary('jperla_HW1.txt');
set(0,'DefaultFigureVisible','off');
% Joseph Perla
% Econ 512A
% Homework 1

%% Question 1: 
X = [1, 1.5, 3.5, 4.78, 5, 7.2, 9, 10, 10.1, 10.56]; % Given vector
Y1 = -2 + 0.5*X; 
Y2 = -2 + 0.5*X.^2;
plot(X,Y1,'g',X,Y2,'b'); % Create Graph
title('Question 1: Y1, Y2 Against X');
ylabel('Y_i');
xlabel('X');
legend('Y1','Y2','Location','northeast');
saveas(gcf,'Q1_graph.png');
%% Question 2: Create a 120x1 vector X containing evenly-spaced numbers starting between 20 and 40. Calculate the sum of the elements of the vector
X = linspace(-20,40,120);
X_sum = sum(X);

%% Question 3: 
A = [3,4,5;2,12,6;1,7,4];

b = [3;-2;10];

C = A'*b;

D = (inv(A'*A))*b;

E = 0;
for i = 1:3;
    for j = 1:3;
        E = sum(A*b);
    end;
end;
% this loop does the same thing every time. you should have written just E = sum(A'*b); 

F = A([1,3],[1,2]);

% Solve the system of linear equations Ax = b for the vector x 
x = linsolve(A,b);

%% Question 4:
O = zeros(3,3);
B = [A,O,O,O,O;O,A,O,O,O;O,O,A,O,O;O,O,O,A,O;O,O,O,O,A];

%% Question 5:
A = normrnd(6.5,3,[5,3]);
D_5 = zeros(5,3);
for i=1:5;
    for j=1:3;
        if A(i,j)<4;
            D_5(i,j)=0;
        elseif A(i,j)>04;
            D_5(i,j)=1;
        end;
    end;
end;

%% Question 6:
data = csvread('datahw1.csv');
% comment from TA: csvread replaces NaN with 0 which makes your dataset corrupted. use readtable instead.

firm_ID = data(:,1);
year = data(:,2);
dummy1 = data(:,3);
dummy2 = data(:,4);
prod = data(:,5);
cap = data(:,6);

X_data = [dummy1,dummy2,cap];
Y_data = prod;

mdl = fitlm(X_data,Y_data);
%% Results Print out
disp('Question 1:')
disp('See graph file.')

disp('Question 2:')
disp('Summation of the elements.')
disp(X_sum)

disp('Question 3:')
disp('Part A and B')
disp('x =') 
disp(x)
disp('Part C')
disp(C)
disp('Part D')
disp(D)
disp('Part E')
disp(E)
disp('Part F')
disp(F)

disp('Question 4:')
disp('Display Block Diagnal Matrix')
disp(B)

disp('Question 5:')
disp('A matrix from a normal distribution:')
disp(A)
disp('D matrix')
disp(D_5)

disp('Question 6:')
disp('Coefficients of the Point Estimates')
disp(mdl.Coefficients(:,1))

diary('off');

% Joseph Perla
% Econ 512A
% Homework 3

clc;
clear;

% Load data
data = load('hw3.mat');
y = data.y(:,1);
x = data.X;

n = length(x);
k = size(x,2);
%% Question 1
% 
%% 
% Estimate the parameter vector $\beta$ using maximum likelihood.
% Use as the starting value a vector of zeros.
% 
% Report the estimated parameters for each case, and the number of iterations and function

%%
% (i) FMINUNC without a derivative supplied
%
% <include>poisson_like.m</include>

%Starting values:
beta0 = zeros(6,1); 

%Options for the the Quasi-Newton Method without the derivative:
options1 = optimoptions('fminunc','Display','iter','GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs');

%Quasi-Newton Method (without the derivative):
tic
[beta_star1,fval,exitflag1,output1] = fminunc(@(beta) poisson_like(beta,x,y,n),beta0,options1);
toc

%Display results
disp('Estimates of beta_hat using fminunc without the derivative supplied:')
disp(beta_star1)

disp('Number of iterations without the derivative supplied:')
disp(output1)

%%
% (ii)FMINUNC with a derivative supplied

%Options for the the Quasi-Newton Method with the derivative:
options2 = optimoptions('fminunc','Display','iter','GradObj','on','Algorithm','quasi-newton','HessUpdate','bfgs');

%Quasi-Newton Method (with the derivative):
tic
[beta_star2,fval,exitflag2,output2] = fminunc(@(beta)poisson_like(beta,x,y,n),beta0,options2);
toc

%Display results
disp('Estimates of beta_hat using fminunc with the derivative supplied:')
disp(beta_star2)

disp('Number of iterations with the the derivative supplied:')
disp(output2)


%%
% (iii) Nelder-Mead Method

%Options for the Nelder-Mead Method:
options3 = optimset('Display','iter');
tic
[beta_star3,fval,exitflag3,output3] = fminsearch(@(beta) poisson_like(beta,x,y,n),beta0,options3);
toc
% your default 'TolFum' is too low, that's why your solution is not the
% same as in question 1
%Display results
disp('Estimates of beta_hat using Nelder-Mead (fminsearch):')
disp(beta_star3)

disp('Number of iterations with Nelder Mead:')
disp(output3)

%% Question 3
% 
%%
% Now estimate the model using the NLLS method we went over in class.
% Use as the starting value a vector of zeros.

option5 = optimset('Tolfun',1e-100,'MaxIter',1000,'MaxFunEvals',2000000000,'Algorithm','levenberg-marquardt');
tic
[beta_star5,resnorm,residual,exitflag5,output5]=lsqnonlin(@(beta) nlls(beta,x,y,n),beta0,[],[],option5);
toc
% your criterion function is incorrect, see answer key
disp('Estimates of beta_hat using NNLS (lsqnonlin):')
disp(beta_star5)

disp('Number of iterations with NNLS:')
disp(output5)

%% Question 1 and 2: BHHH
% 

%%
% Comments: I don�t know why Question 1 part (ii) is not working
% properly. I�ve debugged it with my study partner and we can�t figure out
% the function does not have any iterations. For Questions 3, NLLS that I
% coded seems to be sensitive to the initial starting values and the number
% of iterations. As I increase the iterations, the values seem to converge
% to proper $\beta^*$. Lastly, my BHHH does not work. The examples I have
% found online were not too helpful. My code for this questions was adopted
% from online. Published without BHHH output The m-file will include my
% BHHH code.

%%
% (iv) BHHH maximum likelihood method

iter = 1;          % Initialize iteration count
iter_lim = 100;    % Iteration limit    
s_star = 1;         % Initialize step length

while iter < iter_lim
    
    for i = 1:n
        fval_i(i) = -exp(x(i,:)*beta0) + y(i)*x(i,:)*beta0 - log(factorial(y(i)));
        %z_i = 1/exp(x(i,:)*beta0)*x(i,:) - n;
        z_i = gradient(beta0,fval_i(i),6);    % Numerical Gradients
        z(n,:) = z_i;
    end
    
    % Calculate the Hessian matrix
    H = z'*z;           % Compute approx. to Hessian
    G = sum(z)';        % Gradient vector (K x 1)    

    db = (H\G)/n;           % Compute full step adjustment       
    s = 1;              % Reset base step length?           
    star=s;
    s = s/2; 
    end 
    beta_star4 = beta0 + s_star*db;               % Update parameter values

%Display results
disp('Estimates of beta_hat using BHHH:')
disp(beta_star4)

%%
% The above code was adapted from the University of Wisconsin-Madison.

%% Question 2
% 
%%
% Report the eigenvalues for the Hessian approximation for the BHHH MLE
% method from the last question.

%%
% Report the eigenvalues for the initial Hessian, and the Hessian at the
% estimated parameters.

e1 = eig(H);

%Calculate final hessian
for i = 1:n
        fval_i(i) = -exp(x(i,:)*beta_star4) + y(i)*x(i,:)*beta_star4 - log(factorial(y(i)));
        z_i = gradient(beta_star4,fval_i(i),6);    % Numerical Gradients
        z(n,:) = z_i;
end

HF = z'*z;           % Compute approx. to Hessian
e2 = eig(HF);

disp('Eigenvalues for the Hessian approximation of H1:')
disp(e1)

disp('Eigenvalues for the Hessian approximation of H final:')
disp(e2)


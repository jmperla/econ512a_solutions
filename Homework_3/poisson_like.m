function [f,g] = poisson_like(beta,x,y,n)
% Purpose:   Calculate the log-likelihood, gradient for a Poisson.
% Inputes:   Parameters b and data Y

    for i = 1:n
        fval_j(i) = -exp(x(i,:)*beta) + y(i)*x(i,:)*beta - log(factorial(y(i)));
    
        % gradient g
        g_i = (1/exp(x(i,:)*beta))*x(i,:);
        % incorrect derivative, everything else stems from it try to turn
        % on derivative check when supplying derivative. it may help
        % identify problems
        
        gi(n,:) = g_i;
    end

    f = -sum(fval_j);
    g = sum(gi,1) - n;

end
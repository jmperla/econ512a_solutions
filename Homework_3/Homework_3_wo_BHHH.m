% Joseph Perla
% Econ 512A
% Homework 3

clc;
clear;

% Load data
data = load('hw3.mat');
y = data.y(:,1);
x = data.X;

n = length(x);
k = size(x,2);
%% Question 1
% 
%% 
% Estimate the parameter vector $\beta$ using maximum likelihood.
% Use as the starting value a vector of zeros.
% 
% Report the estimated parameters for each case, and the number of iterations and function

%%
% (i) FMINUNC without a derivative supplied
%
% <include>poisson_like.m</include>

%Starting values:
beta0 = zeros(6,1); 

%Options for the the Quasi-Newton Method without the derivative:
options1 = optimoptions('fminunc','Display','iter','GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs');

%Quasi-Newton Method (without the derivative):
tic
[beta_star1,fval,exitflag1,output1] = fminunc(@(beta) poisson_like(beta,x,y,n),beta0,options1);
toc

%Display results
disp('Estimates of beta_hat using fminunc without the derivative supplied:')
disp(beta_star1)

disp('Number of iterations without the derivative supplied:')
disp(output1)

%%
% (ii)FMINUNC with a derivative supplied

%Options for the the Quasi-Newton Method with the derivative:
options2 = optimoptions('fminunc','Display','iter','GradObj','on','Algorithm','quasi-newton','HessUpdate','bfgs');

%Quasi-Newton Method (with the derivative):
tic
[beta_star2,fval,exitflag2,output2] = fminunc(@(beta)poisson_like(beta,x,y,n),beta0,options2);
toc

%Display results
disp('Estimates of beta_hat using fminunc with the derivative supplied:')
disp(beta_star2)

disp('Number of iterations with the the derivative supplied:')
disp(output2)


%%
% (iii) Nelder-Mead Method

%Options for the Nelder-Mead Method:
options3 = optimset('Display','iter');
tic
[beta_star3,fval,exitflag3,output3] = fminsearch(@(beta) poisson_like(beta,x,y,n),beta0,options3);
toc

%Display results
disp('Estimates of beta_hat using Nelder-Mead (fminsearch):')
disp(beta_star3)

disp('Number of iterations with Nelder Mead:')
disp(output3)

%% Question 3
% 
%%
% Now estimate the model using the NLLS method we went over in class.
% Use as the starting value a vector of zeros.
%
% <include>nlls.m</include>

option5 = optimset('Tolfun',1e-100,'MaxIter',3000,'MaxFunEvals',2000000000,'Algorithm','levenberg-marquardt');
tic
[beta_star5,resnorm,residual,exitflag5,output5]=lsqnonlin(@(beta) nlls(beta,x,y,n),beta0,[],[],option5);
toc

disp('Estimates of beta_hat using NNLS (lsqnonlin):')
disp(beta_star5)

disp('Number of iterations with NNLS:')
disp(output5)

%% Question 1 and 2: BHHH
% 

%%
% Comments: I don�t know why Question 1 part (ii) is not working
% properly. I�ve debugged it with my study partner and we can�t figure out
% why the function does not have any iterations. For Questions 3, NLLS that 
% I coded seems to be sensitive to the initial starting values and the 
% number % of iterations. As I increase the iterations, the values seem to 
% converge % to proper $\beta^*$. Lastly, my BHHH does not work. The 
% examples I have % found online were not too helpful. My code for this 
% questions was adopted from online. Published without BHHH output The 
% m-file will include my
% BHHH code.




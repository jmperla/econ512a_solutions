function [f2] = nlls(beta,x,y,n)
% Purpose:   Calculate the NLLS.
% Inputes:   Parameters b and data Y

    for i = 1:n
        fval_l(i) = (y(i) - exp(x(i,:)*beta))^2;
        % here is a mistake should be (y(i) - exp(x(i,:)*beta))'*(y(i) - exp(x(i,:)*beta))
        % see answer key
    end

    f2 = sum(fval_l);

end
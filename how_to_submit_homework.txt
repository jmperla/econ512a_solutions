1) Set up your Bitbucket account by going to bitbucket.org. Make sure to use your PSU email do that you get unlimited space for free.



2) Set up your local git repository in the folder where you will be working on your homework. Make sure to add all the files that are related to your homework to git tracking. 



3) Set your local repository to track remote repository on your bitbucket remote server. Make sure that remote repository is public or that I have access to it. My login there is rji5040@psu.edu. You can find details about setting things up in the book called ProGit that I have added to lecture materials.



4) Commit as you work on your homework or commit after you have done everything. You will find convenient to commit after each problem solved or more often, but it is always up to you.

5) Push all your commits to remore repository and give me the link to it so that I can check that out and give you grades.

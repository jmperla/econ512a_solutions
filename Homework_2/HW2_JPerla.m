%% Econ 512A: Homework 2
% Joseph Perla
%% Define the Problem
%
%%
J = 3;
v = -1*ones(J,1);
p = ones(J,1);

%% Question 1 
%
%%
% Consider the following parameteriztion $\nu_j = -1\ \forall j$.

% Consumer Demand for each case: A, B, C, and the outside option
denom = 1 + exp(v(1)-p(1)) + exp(v(2)-p(2)) + exp(v(3)-p(3));

q_A = exp(v(1)-p(1))/denom;
q_B = exp(v(2)-p(2))/denom;
q_C = exp(v(3)-p(3))/denom;
q_0 = 1/denom;

q_1 = [q_A; q_B; q_C; q_0];
disp('Question 1 Results: Demand for each option')
disp(q_1)
%% Question 2
%
%% 
% Given the above parameterizations for product values, use Broyden?s
% Method to solve for the Nash pricing equilibrium.

% Initial values
for part_switch = 1:4
    if part_switch ==1 % part a
        p0 = [1,1,1]; 
        part='a';
    elseif part_switch ==2 % part b
        p0 = [0,0,0];
        part='b';
    elseif part_switch ==3 % part c
        p0 = [0,1,2];
        part='c';
    elseif part_switch ==4 % part d
        p0 = [3,2,1];
        part='d';
    end

tic    
% Use Broyden to solve for equilibrium
[p_star_broyden,~,flag] = broyden('EDF',p0');
toc;

%Result:
q2 = sprintf('Question 2 Results: The optimal level of p for each company, part %s',part);
disp(q2)
disp(p_star_broyden) 
    if flag == 0 
        disp('Successfully converges.');
    else
        disp('Fail to converge.');
    end
end
%% Question 3
%
%%
% Use a Guass-Sidel method (using the secant method for each sub-iteration)
% to solve for the pricing equilibirum.
%
% Which method is faster and why do you think?
% Guass-Sidel method should be faster since this method uses $x^{n}$ as
% soon the it is calculated to in order to calculate the next $x^{n+1}$.

%Define the problem
J = 3; % 1 is A, 2 is B and 3 is C.
v = -1*ones(J,1);
p = ones(J,1);
        
% Quantity demanded in each case:
denom = 1 + exp(v(1)-p(1)) + exp(v(2)-p(2)) + exp(v(3)-p(3));
q_A = exp(v(1)-p(1))/denom; 
q_B = exp(v(2)-p(2))/denom;
q_C = exp(v(3)-p(3))/denom;
    
% Define vector x:
for part_switch = 1:4
    if part_switch ==1 % part a
        x = [1;1;1]; 
        part='a';
    elseif part_switch ==2 % part b
        x = [0;0;0];
        part='b';
    elseif part_switch ==3 % part c
        x = [0;1;2];
        part='c';
    elseif part_switch ==4 % part d
        x = [3;2;1];
        part='d';
    end
% Define matrix A: 
A = diag([q_A,q_B,q_C]-1);

% Define vector b:
b = -1*ones(3,1);

% Use Gseidel to solve for equilibrium:
tic
% COmment from TA: you are solving linear system by tyoing in gseidel, but
% your system is essentially nonlinear. You solved just an approximation to
% it
[p_star_gseidel,flag] = gseidel(A,b,x);
toc;

% Result:
q3 = sprintf('Question 3 Results: The optimal level of p for each company, part %s',part);
disp(q3)
disp(p_star_gseidel) 
    if flag == 0 
        disp('Successfully converges.');
    else
        disp('Fail to converge.');
    end
end

%% Question 4
%
%%
% Use the following update rule, $p^{t+1} = \frac{1}{1 - q(p^{t})}$ , to solve for equilibrium

%using the same starting values as in question 2.

% Comment from TA: you got idea of update wrong. You should have had used
% simple replacement of guesses based on provided formula without any
% optimization. See answer key online. 
options = optimset('Diagnostics','on','MaxFunEvals',15000000, 'MaxIter',100000,'TolFun', 0.00001);
[p_star_4,~,exitflag] = fsolve(@(p)EDF4(p),p0,options);
q4 = sprintf('Question 4 Results: The optimal level of p for each company, part %s',part);
disp(q4)
disp(p_star_4) 
	if exitflag == 1 
        disp('Successfully converges.');
    else
        disp('Fail to converge.');
	end    

%% Question 5
%
%%
% Define the problem
J = 3;
v_A = -1;
v_B = -1;
v_C = [-4,-3,-2,-1, 0, 1];
p = ones(J,1);
v = [repmat([v_A;v_B],[1,length(v_C)]);v_C];
   
% Intitial values:
x5_1 = [1;1;1];             
x5_2 = [0;0;0];          
x5_3 = [0;1;2];           
x5_4 = [3;2;1];

L = size(v,2);
	for l = 1:L    
        denom = 1 + exp(v(1,l)-p(1)) + exp(v(2,l)-p(2)) + exp(v(3,l)-p(3));
        q_A = exp(v(1,l)-p(1))/denom; 
        q_B = exp(v(2,l)-p(2))/denom;
        q_C = exp(v(3,l)-p(3))/denom;
        
        % Define matrix A: 
        A5 = diag([q_A,q_B,q_C]-1);
    
        % Define vector b:
        b5 = -1*ones(3,1);
                
        tic
        % Use Gseidel to solve for equilibrium:              
        [p_star_5_1(:,l),flag_1] = gseidel(A5,b5,x5_1);
        [p_star_5_2(:,l),flag_2] = gseidel(A5,b5,x5_2);
        [p_star_5_3(:,l),flag_3] = gseidel(A5,b5,x5_3);
        [p_star_5_4(:,l),flag_4] = gseidel(A5,b5,x5_4);
        toc;            
	end
    
% Result:
% part a:
q5_1 = sprintf('Question 5 Results: The optimal level of p for each company, part a');
disp(q5_1)
disp(p_star_5_1) 
    if flag_1 == 0 
        disp('successfully converges');
    else
         disp('fail to converge');
    end           
averageTime_1 = toc/L;
Avg_time_1 = sprintf('Average Elapsed Time, part a: %6d.', averageTime_1);
disp(Avg_time_1)
    
figure;
plot(p_star_5_1(1,:),v_C,p_star_5_1(3,:),v_C);
xlabel('p');
ylabel('vC');
legend('pA','pB','Location','northeast');
title('p vs. vC, part a');
    
% part b:
q5_2 = sprintf('Question 5 Results: The optimal level of p for each company, part b');
disp(q5_2)
disp(p_star_5_2) 
    if flag_2 == 0 
        disp('successfully converges');
    else
         disp('fail to converge');
    end           
averageTime_2 = toc/L;
Avg_time_2 = sprintf('Average Elapsed Time, part b: %6d.', averageTime_2);
disp(Avg_time_2)

figure;
plot(p_star_5_2(1,:),v_C,p_star_5_2(3,:),v_C);
xlabel('p');
ylabel('vC');
legend('pA','pB','Location','northeast');
title('p vs. vC, part b');
    
    % part c:
q5_3 = sprintf('Question 5 Results: The optimal level of p for each company, part c');
disp(q5_3)
disp(p_star_5_3) 
    if flag_3 == 0 
        disp('successfully converges');
    else
         disp('fail to converge');
    end           
averageTime_3 = toc/L;
Avg_time_3 = sprintf('Average Elapsed Time, part c: %6d.', averageTime_3);
disp(Avg_time_3)

figure;
plot(p_star_5_3(1,:),v_C,p_star_5_3(3,:),v_C);
xlabel('p');
ylabel('vC');
legend('pA','pB','Location','northeast');
title('p vs. vC, part c');
    
    % part d:
q5_4 = sprintf('Question 5: optimal level of p for each company, part d');
disp(q5_4)
disp(p_star_5_4) 
    if flag_4 == 0 
        disp('successfully converges');
    else
         disp('fail to converge');
    end           
averageTime_4 = toc/L;
Avg_time_4 = sprintf('Average Elapsed Time, part d: %6d.', averageTime_4);
disp(Avg_time_4)

figure;
plot(p_star_5_4(1,:),v_C,p_star_5_4(3,:),v_C);
xlabel('p');
ylabel('vC');
legend('pA','pB','Location','northeast');
title('p vs. vC, part d');

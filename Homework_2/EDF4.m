function [fval] = EDF4(p)
%Function for using the Broyden Method
%Define v
    J = 3;
    v = -1*ones(J,1);
    
%Consumer Demand for each case: A, B, C, and the outside option
    max_count = 100;
    for t = 1:max_count
        p_T = p.^t;
        denom = 1 + exp(v(1)-p_T(1)) + exp(v(2)-p_T(2)) + exp(v(3)-p_T(3));
        q_A_T = exp(v(1)-p_T(1))/denom;
        q_B_T = exp(v(2)-p_T(2))/denom;
        q_C_T = exp(v(3)-p_T(3))/denom;
        q_T = [q_A_T;q_B_T;q_C_T];
        fval = (p.^(1+t)) - 1/(1-q_T);
    end
end


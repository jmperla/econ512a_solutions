function [fval] = EDF(p)
%Function for using the Broyden Method
%Define v
    J = 3;
    v = -1*ones(J,1);
    
%Consumer Demand for each case: A, B, C, and the outside option
    denom = 1 + exp(v(1)-p(1)) + exp(v(2)-p(2)) + exp(v(3)-p(3));
    q_A = exp(v(1)-p(1))/denom;
    q_B = exp(v(2)-p(2))/denom;
    q_C = exp(v(3)-p(3))/denom;
    q = [q_A,q_B,q_C];
    fval = q'.*p -(p-1);
end
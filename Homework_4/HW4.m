% Joseph Perla
% Econ 512A
% Homework 3

%%
%
% Load Data

function HW4
clc;
clear;

%Transpose X, Z, Y in order to have the number of observations on the row
data = load('hw4data.mat');
N = data.data.N;
T = data.data.T;
X = data.data.X';
Y = data.data.Y';
Z = data.data.Z';
clear data;

beta0 = 0.1;
u0 = 0;
gamma = 0;

sig_b = 1;
sig_bu = 0;
sig_ub = sig_bu;
sig_u = 0;

mu = [beta0 ; u0];
Sigma = [sig_b , sig_bu; sig_ub, sig_u];

%% Question 1
%
%%
% Use Gaussian Quadrature using 5 nodes to calculate the likelihood
% function.

[beta_node, w] = qnwnorm(5, mu(1), Sigma(1));
beta_node5 = repmat(beta_node,N/5);
beta_node5 = beta_node5(:,1);
w = repmat(w,N/5);
w = w(:,1)./(N/5);
% Comment from TA: I don't get why are you doing all this 4 lines?

for t = 1:T
    for i = 1:N
    u(i) = 0;
    epsilon(i,t) = beta_node5(i)*X(i,t)+gamma*Z(i,t)+u(i);
    F(i,t) = (1 + exp(-epsilon(i,t)))^(-1);
    L(i,t) = (F(i,t))^(Y(i,t))*(1-F(i,t))^(1-Y(i,t))*normpdf(beta_node5(i),mu(1),Sigma(1));
    % comment from TA: what does normpdf appears here?
    end
    
    LL(i) = prod(L(i,t));
end
% Comment from TA: this procedure is definitely not what you want to do,
% take a look at answer key. Here you are 
int_5node = sum(LL*w)
%% Question 2
%
%%
% Use Gaussian Quadrature using 10 nodes to calculate the likelihood
% function.

[beta_node, w] = qnwnorm(10, mu(1), Sigma(1));
beta_node10 = repmat(beta_node,N/10);
beta_node10 = beta_node10(:,1);
w = repmat(w,N/10);
w = w(:,1)./(N/10);

for t = 1:T
    for i = 1:N
    u(i) = 0;
    epsilon(i,t) = beta_node10(i)*X(i,t)+gamma*Z(i,t)+u(i);
    
    F(i,t) = (1 + exp(-epsilon(i,t)))^(-1);
    L(i,t) = (F(i,t))^(Y(i,t))*(1-F(i,t))^(1-Y(i,t))*normpdf(beta_node10(i),mu(1),Sigma(1));
    
    end
    LL(i) = prod(L(i,t));
end

int_10node = sum(LL*w)

% comment from TA: this is also not correst integration, see answer key
%% Question 3
%
%%
% Use Monte Carlo Methods using 20 node to calculate the likelihood
% function.

N1 = 100;
beta_mc = random('Normal',mu(1),Sigma(1),100,1); 

for t = 1:T
    for i = 1:N1
        u(i) = 0;
        epsilon(i,t) = beta_mc(i)*X(i,t) + gamma*Z(i,t) + u(i);
        
        F(i,t) = (1+exp(-epsilon(i,t)))^(-1);
        L(i,t) = (F(i,t)^(Y(i,t)))*(1-F(i,t))^(1-Y(i,t))*normpdf(beta_mc(i),mu(1),Sigma(1));
        % comment from TA: you are supposed to integrate here, you are not
        % integrating anything. you are giving each individual his own
        % beta(i)
        LL(i) = prod(L(i,t));
    end    
end

int_3 = sum(LL)/N1
%% Question 4
%
%%
% Maximize (or minimize the negative) likelihood function with respect to
% the parameters using all three integration techniques above. Use Matlab�s
% fmincon without a supplied derivative to max (min) your objective
% function.

A = ones(N,N);
b = Inf*ones(N,1);
n1 = 5;
n2 = 10;

[beta_node, w] = qnwnorm(n1, mu(1), Sigma(1));
beta0 = repmat(beta_node,N/n1);
beta0 = beta0(:,1);
w = repmat(w,N/n1);
w = w(:,1)./(N/n1);

options = optimoptions(@fmincon,'Display','iter','TolFun',1e-20,'TolX',1e-20);
[beta_star,fval,exitflag] = fmincon(@(beta)likehood_opt(beta,X,Y,Z,N,T,gamma,Sigma,mu,n1,n2,w),beta0,A,b,[],[],[],[],[],options);
disp('Starting values for fmincon using 5 nodes:')
disp(beta_node)
disp('Optimal betas for 5 nodes:')
disp(beta_star)
disp('Maximized value of the Likelihood function using 5 nodes:')
disp(fval)

[beta_node, w] = qnwnorm(n2, mu(1), Sigma(1));
beta0 = repmat(beta_node,N/n2);
beta0 = beta0(:,1);
w = repmat(w,N/n2);
w = w(:,1)./(N/n2);

options = optimoptions(@fmincon,'Display','iter','TolFun',1e-20,'TolX',1e-20);
[beta_star,fval,exitflag] = fmincon(@(beta)likehood_opt2(beta,X,Y,Z,N,T,gamma,Sigma,mu,n1,n2,w),beta0,A,b,[],[],[],[],[],options);
disp('Starting values for fmincon using 10 nodes:')
disp(beta_node)
disp('Optimal betas for 10 nodes:')
disp(beta_star)
disp('Maximized value of the Likelihood function using 10 nodes:')
disp(fval)

beta0 = random('Normal',mu(1),Sigma(1),100,1);
options = optimoptions(@fmincon,'Display','iter','TolFun',1e-10,'TolX',1e-6);
[beta_star,fval2,exitflag] = fmincon(@(beta)likehood_opt2(beta,X_data,Y,Z_data,N,T,gamma,Sigma,mu),beta0,A,b,[],[],[],[],[],options);        
disp('Starting values for fmincon using Monte Carlo Methods:')
disp(beta0)
disp('Optimal betas using Monte Carlo methods:')
disp(beta_star)
disp('Maximized value of the Likelihood function using Monte Carlo methods:')
disp(fval2)

%% Question 5
%
%%
% Maximize the likelihood function, estimating all of the parameters, using
% Monte Carlo methods.

N1 = 100;
u0 = 0.2;
sig_u = u0;
sig_ub = u0;
Sigma = [sigma_b,sig_ub;sig_ub,sig_u];
Sigma_rep = repmat([sigma_b;sig_ub;sig_u],N);
Sigma_rep = Sigma_rep(:,1);
mu = [0.1,u0];
coeff = mvnrnd(mu,Sigma,N1);
params0 = [coeff(:,1);coeff(:,2);Sigma_rep];
        
options = optimoptions(@fmincon,'Display','iter','TolFun',1e-10,'TolX',1e-6);
[params_star,fval5,exitflag] = fmincon(@(params)likehood5(params,X_data,Y,Z_data,N,T,gamma,Sigma,mu),params0,A5,b5,[],[],[],[],[],options);        
disp('Starting values for fmincon using Monte Carlo Methods:')
disp(coeff)
disp('Optimal betas using Monte Carlo methods:')
disp(params_star)
disp('Maximized value of the Likelihood function using Monte Carlo methods:')
disp(fval5)          

end
%%
%

%%
% Likelihood function for Question 4 and 5
function [fval] = likehood_opt(beta,X,Y,Z,N,T,gamma,Sigma,mu,n1,n2,w)

for t = 1:T
    for i = 1:N
        u(i) = 0;
        epsilon(i,t) = beta(i)*X(i,t) + gamma*Z(i,t) + u(i);
        F(i,t) = (1+exp(-epsilon(i,t)))^(-1);
        L(i,t) = (F(i,t)^(Y(i,t)))*(1-F(i,t))^(1-Y(i,t))*normpdf(beta(i),mu(1),Sigma(1));
        LL(i) = prod(L(i,t));
    end    
 end
    
fval = -sum(L*w);   
end

function [fval2] = likehood_opt2(beta,X,Y,Z,N,T,gamma,Sigma,mu,n1,n2,w)

for t = 1:T
    for i = 1:N
        u(i) = 0;
        epsilon(i,t) = beta(i)*X(i,t) + gamma*Z(i,t) + u(i);
        F(i,t) = (1+exp(-epsilon(i,t)))^(-1);
        L(i,t) = (F(i,t)^(Y(i,t)))*(1-F(i,t))^(1-Y(i,t))*normpdf(beta(i),mu(1),Sigma(1));
        LL(i) = prod(L(i,t));
    end
end
    
fval2 = -sum(L)/N;
end

function [fval5] = likehood_opt5(params,X,Y,Z,N,T,gamma,Sigma,mu) 
beta = params(1:100);
u = params(101:200);
sig_b = params(201:300);
sig_ub = params(301:400);
sig_u = params(401:500);    
    
for i = 1:N
    joint_density(i) = mvnpdf([params(1:100),params(101:200)],mu,[sig_b(i),sig_ub(i);sig_ub(i),sig_u(i)]);
end

for t = 1:T
	for i = 1:N1
        u(i) = 0;
        epsilon(i,t) = beta(i)*X(i,t) + gamma*Z(i,t) + u(i);
        F(i,t) = (1+exp(-epsilon(i,t)))^(-1);
        L_ind(i,t) = (F(i,t)^(Y(i,t)))*(1-F(i,t))^(1-Y(i,t))*joint_density(i);
        L(i) = prod(L_ind(i,t));
	end    
end

int_5 = sum(L)/N1;
end